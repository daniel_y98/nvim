# Nvim Configuration

Plugins will automatically install after neovim is opened with this config.

## Dependencies

### Fonts

Some are probably missing.

- Iosevka: https://aur.archlinux.org/packages/ttf-iosevka/

- Nerd Fonts: https://aur.archlinux.org/packages/nerd-fonts-complete/

